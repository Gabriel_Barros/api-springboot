create table public.lancamento(
    codigo bigserial primary key,
    descricao varchar(50) not null,
    data_vencimento date not null,
    data_pagamento date,
    valor numeric(10,2) not null,
    observacao varchar(100),
    tipo varchar(20) not null,
    codigo_categoria BIGINT not null,
    codigo_pessoa BIGINT not null,
    foreign key(codigo_categoria) references public.categoria(codigo),
    foreign key(codigo_pessoa) references public.pessoa(codigo)
);

insert into public.lancamento(descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values
('Salário Mensal', '2019-08-31', null, 2800.00, 'Distribuição de lucros', 'RECEITA', 1, 1),
('Carro', '2019-08-05', null, 1200.00, 'Prestação do carro', 'DESPESA', 3, 3),
('Eletrônicos', '2019-07-06', '2019-07-04', 450.00, null, 'DESPESA', 2, 5),
('Lanche', '2019-07-10', '2019-06-30', 300.00, null, 'DESPESA', 4, 2),
('Extra', '2019-09-28', null, 2000.00, null, 'DESPESA', 5, 4);